package com.osustats.view;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.osustats.model.Constants;
import com.osustats.osustats.R;

public class InitActivity extends AppCompatActivity {

    // private references
    private EditText mPlayerNameView;
    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sp = PreferenceManager.getDefaultSharedPreferences(this);
        String playerName = sp.getString(Constants.PLAYER_NAME, "");
        if (playerName.isEmpty()) {
            // First start of the app
            setContentView(R.layout.activity_init);
            mPlayerNameView = (EditText) findViewById(R.id.init_player_name);
            Button mStartButton = (Button) findViewById(R.id.init_start_button);
            mStartButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    attemptStart();
                }
            });
        } else {
            // Second+ start of the app, already have shared preferences file
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private void attemptStart() {
        // Ensure player name field is not empty
        if (mPlayerNameView.getText().length() == 0) {
            Toast.makeText(InitActivity.this, R.string.error_field_required, Toast.LENGTH_SHORT).show();
            return;
        }
        // Save shared preferences
        String playerName = mPlayerNameView.getText().toString();
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(Constants.PLAYER_NAME, playerName);
        editor.putInt(Constants.MODE, Constants.MODE_STD);
        editor.apply();
        // Start main activity
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(Constants.FIRST_START, true);   // purpose: hide some UIs for app's first start if connection problem or user name not found
        startActivity(intent);
        finish();
    }

}

