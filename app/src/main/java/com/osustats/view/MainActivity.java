package com.osustats.view;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;

import com.osustats.adapter.TabsAdapter;
import com.osustats.fragment.FindFragment;
import com.osustats.fragment.ProfileFragment;
import com.osustats.interfaces.ActivityFragmentCommunication;
import com.osustats.model.Constants;
import com.osustats.osustats.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ActivityFragmentCommunication {

    private ViewPager m_viewPager;
    private ImageButton m_refreshButton;
    private boolean m_isFirstStart = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(new ProfileFragment());
        fragmentList.add(new FindFragment());
        m_viewPager = (ViewPager) findViewById(R.id.viewpager);
        TabsAdapter tabsAdapter = new TabsAdapter(getSupportFragmentManager(), fragmentList);

        m_refreshButton = (ImageButton) findViewById(R.id.refresh_button);
        // Hide soft keyboard when flipping to a different page
        // Hide refresh button on page 2
        m_viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                if (position != 0) {
                    m_refreshButton.setVisibility(View.GONE);
                } else {
                    m_refreshButton.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrolled(int position, float offset, int offsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(m_viewPager.getWindowToken(), 0);
                }
            }
        });

        m_viewPager.setAdapter(tabsAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(m_viewPager);
        // set tab icon colors
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_profile);
        tabLayout.getTabAt(0).getIcon().setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorTabFocused), PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_find);
        tabLayout.getTabAt(1).getIcon().setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorTabUnfocused), PorterDuff.Mode.SRC_IN);
        // set tab icon listener
        tabLayout.addOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(m_viewPager) {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        super.onTabSelected(tab);
                        int tabIconColor = ContextCompat.getColor(MainActivity.this, R.color.colorTabFocused);
                        tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        super.onTabUnselected(tab);
                        int tabIconColor = ContextCompat.getColor(MainActivity.this, R.color.colorTabUnfocused);
                        tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
                    }
                }
        );

        m_isFirstStart = getIntent().getBooleanExtra(Constants.FIRST_START, false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            FindFragment settingsDialog = new FindFragment();
////            settingsDialog.setController(mController);
//            settingsDialog.show(getFragmentManager(), "Change Settings");
//            return true;
//        } else
        if (id == R.id.action_customize) {
            // Opens customize menu item
            // Prevent settings from showing headers
            Intent intent = new Intent(this, CustomizeActivity.class);
            intent.putExtra(PreferenceActivity.EXTRA_SHOW_FRAGMENT, CustomizeActivity.GeneralPreferenceFragment.class.getName());
            intent.putExtra(PreferenceActivity.EXTRA_NO_HEADERS, true);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    private Fragment findFragmentByPosition(int position) {
        return getSupportFragmentManager().findFragmentByTag("android:switcher:" + m_viewPager.getId() + ":" + position);
    }

    @Override
    public void onSearch() {
        ((FindFragment) findFragmentByPosition(1)).disableFavouritesList();
        ((ProfileFragment) findFragmentByPosition(0)).fetchData();
    }

    // player name is the staging one if name not found or connection problem, otherwise it is server reply's player name
    // same with player id
    @Override
    public void onSearchFinished(ProfileFragment.ResponseStatusForPlayerName responseStatusForPlayerName, String playerName, String playerId) {
        switch (responseStatusForPlayerName) {
            case FOUND:
                if (m_isFirstStart) {
                    m_isFirstStart = false;
                    ((ProfileFragment) findFragmentByPosition(0)).toggleUIForFirstStart(true);
                }
                ((FindFragment) findFragmentByPosition(1)).updatePlayerInFavourites(true, playerName, playerId);
                break;
            case NOTFOUND:
                if (m_isFirstStart) {
                    // hide some UIs for app's first start if connection problem or user name not found
                    ((ProfileFragment) findFragmentByPosition(0)).toggleUIForFirstStart(false);
                }
                ((FindFragment) findFragmentByPosition(1)).updatePlayerInFavourites(false, playerName, playerId);
                break;
            case UNKNOWN:
                if (m_isFirstStart) {
                    // hide some UIs for app's first start if connection problem or user name not found
                    ((ProfileFragment) findFragmentByPosition(0)).toggleUIForFirstStart(false);
                }
                break;
        }
        ((FindFragment) findFragmentByPosition(1)).enableFavouritesList();
    }

    @Override
    public void updateFavouritesIcon(boolean isFavourited) {
        ((ProfileFragment) findFragmentByPosition(0)).updateFavouritesIcon(isFavourited);
    }

    @Override
    public void onFavouritesIconClicked(boolean isFavourited, String playerName, String playerId) {
        ((FindFragment) findFragmentByPosition(1)).updateFavouritesList(isFavourited, playerName, playerId);
    }

    @Override
    public boolean isPlayerDataInFavouritesList(String playerName, String playerId) {
        return ((FindFragment) findFragmentByPosition(1)).isPlayerDataInFavouritesList(playerName, playerId);
    }

}
