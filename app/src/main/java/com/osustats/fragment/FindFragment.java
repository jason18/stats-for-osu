package com.osustats.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.osustats.adapter.FavRecyclerViewAdapter;
import com.osustats.controller.Controller;
import com.osustats.interfaces.ActivityFragmentCommunication;
import com.osustats.model.Constants;
import com.osustats.model.MySpinner;
import com.osustats.osustats.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

/**
 * Created by Jason on 2/5/2016.
 */
public class FindFragment extends android.support.v4.app.Fragment {

    private SharedPreferences m_sp;
    private Controller m_controller;
    private EditText m_et_find_playername;
    private EditText m_et_fav_playername;
    private FavRecyclerViewAdapter m_rva_favourites;
    private ActivityFragmentCommunication m_parentActivityCallback;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        m_parentActivityCallback = (ActivityFragmentCommunication) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        m_controller = new Controller();
        m_sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_find, container, false);
        RecyclerView rv_favourites = (RecyclerView) view.findViewById(R.id.rvFavourites);
        rv_favourites.setNestedScrollingEnabled(false);
        rv_favourites.setLayoutManager(new LinearLayoutManager(getActivity()));
//        rv_favourites.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

        // Load favourites list from shared preferences
        // If using (legacy) string array to store favourites, we convert it to playerData array first
        Gson gson = new Gson();
        FavRecyclerViewAdapter.PlayerData[] favPlayerData;
        String favPlayerDataJSON = m_sp.getString(Constants.FAVOURITES, "");
        try {
            favPlayerData = gson.fromJson(favPlayerDataJSON, FavRecyclerViewAdapter.PlayerData[].class);
        } catch (JsonSyntaxException e) {
            String[] legacyFavPlayerNames = gson.fromJson(favPlayerDataJSON, String[].class);
            ArrayList<FavRecyclerViewAdapter.PlayerData> tempArrayList = new ArrayList<>();
            for (String legacyFavPlayerName : legacyFavPlayerNames) {
                FavRecyclerViewAdapter.PlayerData playerData = new FavRecyclerViewAdapter.PlayerData(legacyFavPlayerName, "");
                tempArrayList.add(playerData);
            }
            favPlayerData = tempArrayList.toArray(new FavRecyclerViewAdapter.PlayerData[tempArrayList.size()]);
        }

        if (favPlayerData != null) {
            m_rva_favourites = new FavRecyclerViewAdapter(new LinkedList<FavRecyclerViewAdapter.PlayerData>(Arrays.asList(favPlayerData)), m_controller, m_parentActivityCallback);
        } else {
            m_rva_favourites = new FavRecyclerViewAdapter(new LinkedList<FavRecyclerViewAdapter.PlayerData>(), m_controller, m_parentActivityCallback);
        }
        rv_favourites.setAdapter(m_rva_favourites);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        m_et_find_playername = (EditText) getView().findViewById(R.id.etFindPlayerName);
        m_et_fav_playername = (EditText) getView().findViewById(R.id.etFavPlayerName);

        // Add functionality to clear text button (for finding player)
        getView().findViewById(R.id.ibFindClearText).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_et_find_playername.setText("");
            }
        });

        // Add functionality to find button
        ImageButton button = (ImageButton) getView().findViewById(R.id.ibFind);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performFind();
            }
        });

        // Set keyboard to search (instead of enter) when focus is on find player edit text
        ((EditText) getView().findViewById(R.id.etFindPlayerName)).setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performFind();
                    return true;
                }
                return false;
            }
        });

        // Populate recent searches spinner
        MySpinner spinner = (MySpinner) getView().findViewById(R.id.spinnerRecentSearches);
        ArrayAdapter arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, new LinkedList<String>(m_controller.getRecentSearches()));
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
        spinner.setSelection(0, false);   // Prevents selection listener from triggering the first time app starts
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                m_controller.onSettingsChanged(parent.getSelectedItem().toString(), Constants.MODE_NONE);
                m_parentActivityCallback.onSearch();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        // Add functionality to edit button
        final ImageButton editButton = (ImageButton) getView().findViewById(R.id.ibEdit);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View addFavouritesSection = getView().findViewById(R.id.rlAddFavPlayer);
                if (addFavouritesSection.getVisibility() == View.GONE) {
                    // click on edit button
                    addFavouritesSection.setVisibility(View.VISIBLE);
                    m_et_fav_playername.requestFocus();
                    editButton.setImageResource(R.drawable.ic_done_favourites);
                    m_rva_favourites.showButtons();
                } else if (addFavouritesSection.getVisibility() == View.VISIBLE) {
                    // click on done button
                    addFavouritesSection.setVisibility(View.GONE);
                    editButton.setImageResource(R.drawable.ic_edit_favourites);
                    m_rva_favourites.hideButtons();
                    // Close soft keyboard if open
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getActivity().findViewById(R.id.etFindPlayerName).getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
            }
        });

        // Add functionality to clear text button (for adding fav player)
        getView().findViewById(R.id.ibFavClearText).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_et_fav_playername.setText("");
            }
        });

        // Add functionality to add favourites button
        getView().findViewById(R.id.ibAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performAddPlayer();
            }
        });

        // Set keyboard to add (instead of enter) when focus is on add player edit text
        ((EditText) getView().findViewById(R.id.etFavPlayerName)).setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    performAddPlayer();
                    return true;
                }
                return false;
            }
        });

        // Update profile frag's fav icon
        if (!m_controller.getPlayerName().isEmpty()) {
            int index = m_rva_favourites.playerDataContainsIgnoreCase(m_controller.getPlayerName(), "");
            if (index == -1) {
                m_parentActivityCallback.updateFavouritesIcon(false);
            } else {
                m_parentActivityCallback.updateFavouritesIcon(true);
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        FavRecyclerViewAdapter.PlayerData[] favPlayersData = m_rva_favourites.getPlayerDataArray();
        Gson gson = new Gson();
        String favPlayersDataJSON = gson.toJson(favPlayersData, FavRecyclerViewAdapter.PlayerData[].class);
        SharedPreferences.Editor editor = m_sp.edit();
        editor.putString(Constants.FAVOURITES, favPlayersDataJSON);
        editor.apply();
    }

    public void updatePlayerInFavourites(boolean isPlayerNameFound, String playerName, String playerId) {
        m_rva_favourites.updatePlayerInFavourites(isPlayerNameFound, playerName, playerId);
    }

    public void enableFavouritesList() {
        m_rva_favourites.enableClicking();
    }

    public void disableFavouritesList() {
        m_rva_favourites.disableClicking();
    }

    public void updateFavouritesList(boolean isFavourited, String playerName, String playerId) {
        m_rva_favourites.updateFavouritesList(isFavourited, playerName, playerId);
    }

    public boolean isPlayerDataInFavouritesList(String playerName, String playerId) {
        return m_rva_favourites.playerDataContainsIgnoreCase(playerName, playerId) != -1;
    }

    private void performFind() {
        String playername = m_et_find_playername.getText().toString().trim();
        if (playername.isEmpty()) {
            Toast.makeText(getActivity(), "Player name empty", Toast.LENGTH_SHORT).show();
            return;
        }

        // Close soft keyboard if open
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getActivity().findViewById(R.id.etFindPlayerName).getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

        m_controller.onSettingsChanged(playername, Constants.MODE_NONE);
        m_parentActivityCallback.onSearch();
    }

    private void performAddPlayer() {
        String playerNameToAdd = m_et_fav_playername.getText().toString().trim();
        if (playerNameToAdd.isEmpty()) {
            Toast.makeText(getContext(), "Player name empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if (m_rva_favourites.getItemCount() >= Constants.FAVOURITES_MAXIMUM) {
            Toast.makeText(getContext(), "You can only have up to " + Constants.FAVOURITES_MAXIMUM + " favourite players", Toast.LENGTH_SHORT).show();
            return;
        }
        if (m_rva_favourites.playerDataContainsIgnoreCase(playerNameToAdd, "") != -1) {
            Toast.makeText(getContext(), "Player already added", Toast.LENGTH_SHORT).show();
            return;
        }
        m_rva_favourites.addPlayer(playerNameToAdd, "");
        m_et_fav_playername.setText("");
        // reflect change in profile star icon
        if (m_controller.getPlayerName().compareToIgnoreCase(playerNameToAdd) == 0) {
            m_parentActivityCallback.updateFavouritesIcon(true);
        }
    }
}
