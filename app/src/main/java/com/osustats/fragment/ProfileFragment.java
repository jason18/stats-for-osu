package com.osustats.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.osustats.adapter.ScoreRecyclerViewAdapter;
import com.osustats.controller.Controller;
import com.osustats.interfaces.ActivityFragmentCommunication;
import com.osustats.model.Constants;
import com.osustats.model.DataAccess;
import com.osustats.model.MySpinner;
import com.osustats.model.Score;
import com.osustats.osustats.R;

import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

/**
 * Created by Jason on 4/2/2017.
 */

public class ProfileFragment extends android.support.v4.app.Fragment {

    private Controller m_controller;
    private SharedPreferences m_sp;
    private DataAccess m_cachedData = null;
    private boolean m_isFetchDataSuccessful = false;  // value used to update or not update
    private ScoreRecyclerViewAdapter m_scoreRecyclerViewAdapter;
    private boolean m_isFavourited = false;
    private RadioGroup.OnCheckedChangeListener m_onCheckedChangeListener;
    private ActivityFragmentCommunication m_parentActivityCallback;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        m_parentActivityCallback = (ActivityFragmentCommunication) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        m_controller = new Controller(this);
        m_sp = PreferenceManager.getDefaultSharedPreferences(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_profile, container, false);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerViewScores);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        m_scoreRecyclerViewAdapter = new ScoreRecyclerViewAdapter(new Score[0], view);
        recyclerView.setAdapter(m_scoreRecyclerViewAdapter);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Initialize settings
        String playerName = m_sp.getString(Constants.PLAYER_NAME, "");
        int mode = m_sp.getInt(Constants.MODE, Constants.MODE_STD);
        Gson gson = new Gson();
        String recentSearchesJSON = m_sp.getString(Constants.RECENT_SEARCHES, "");
        String[] recentSearches = gson.fromJson(recentSearchesJSON, String[].class);
        m_controller.onSettingsInitialized(playerName, mode, recentSearches);

        getActivity().findViewById(R.id.loadingPanel).setVisibility(View.GONE);
        ImageButton button = (ImageButton) getActivity().findViewById(R.id.refresh_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                m_controller.onSettingsChanged(null, Constants.MODE_NONE);
                m_parentActivityCallback.onSearch();
            }
        });

        // Set radio button to the saved mode
        mode = m_controller.getMode();
        RadioButton radioButton;
        switch (mode) {
            case Constants.MODE_STD:
                radioButton = (RadioButton) getView().findViewById(R.id.osu_std);
                break;
            case Constants.MODE_TAIKO:
                radioButton = (RadioButton) getView().findViewById(R.id.osu_taiko);
                break;
            case Constants.MODE_CTB:
                radioButton = (RadioButton) getView().findViewById(R.id.osu_ctb);
                break;
            case Constants.MODE_MANIA:
                radioButton = (RadioButton) getView().findViewById(R.id.osu_mania);
                break;
            default:
                radioButton = (RadioButton) getView().findViewById(R.id.osu_std);
                break;
        }
        radioButton.toggle();

        // Add click listener to radio buttons
        RadioGroup radioGroup = (RadioGroup) getView().findViewById(R.id.radio_group);
        m_onCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int mode = checkedId;
                switch (mode) {
                    case R.id.osu_std:
                        mode = Constants.MODE_STD;
                        break;
                    case R.id.osu_taiko:
                        mode = Constants.MODE_TAIKO;
                        break;
                    case R.id.osu_ctb:
                        mode = Constants.MODE_CTB;
                        break;
                    case R.id.osu_mania:
                        mode = Constants.MODE_MANIA;
                        break;
                    default:
                        mode = Constants.MODE_STD;
                        break;
                }
                m_controller.onSettingsChanged(null, mode);
                m_parentActivityCallback.onSearch();
            }
        };
        radioGroup.setOnCheckedChangeListener(m_onCheckedChangeListener);

        // Add click listener to favourites button
        final ImageButton ibFavourite = (ImageButton) getView().findViewById(R.id.ibFavourite);
        ibFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (m_isFavourited) {
                    ibFavourite.setImageResource(R.drawable.ic_profile_star_empty);
                } else {
                    ibFavourite.setImageResource(R.drawable.ic_profile_star_filled);
                }
                m_isFavourited = !m_isFavourited;
                if (m_cachedData != null) {
                    try {
                        m_parentActivityCallback.onFavouritesIconClicked(m_isFavourited, m_cachedData.getPlayerName(), m_cachedData.getPlayerId());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    m_parentActivityCallback.onFavouritesIconClicked(m_isFavourited, m_controller.getPlayerName(), m_sp.getString(Constants.PLAYER_ID, ""));
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        // ====================== Update all UI data ==========================
        // I decided to put this code here instead of onActivityCreated because
        // when I change a setting, it'll get reflected right away without refreshing

        // Load cached data if we have it, otherwise fetch from servers
        String scoresJSON = m_sp.getString(Constants.SCORES, "");
        if (!scoresJSON.isEmpty()) {
            Gson gson = new Gson();
            // have player data cached
            // display it directly
            String playerId = m_sp.getString(Constants.PLAYER_ID, "");
            String pp = m_sp.getString(Constants.PP, "");
            String worldRank = m_sp.getString(Constants.WORLD_RANK, "");
            String countryRank = m_sp.getString(Constants.COUNTRY_RANK, "");
            String country = m_sp.getString(Constants.COUNTRY, "");
            String level = m_sp.getString(Constants.LEVEL, "");
            String acc = m_sp.getString(Constants.ACC, "");
            String playCount = m_sp.getString(Constants.PLAY_COUNT, "");
            String totalHits = m_sp.getString(Constants.TOTAL_HITS, "");
            String rankCountsJSON = m_sp.getString(Constants.RANK_COUNTS, "");
            String[] rankCounts = gson.fromJson(rankCountsJSON, String[].class);
            if (rankCounts == null) {
                rankCounts = new String[]{"0", "0", "0"};
            }
            Score[] scores = gson.fromJson(scoresJSON, Score[].class);
            // load display picture
            String encodedDisplayPictureString = m_sp.getString(Constants.DISPLAY_PICTURE, "");
            Bitmap bitmap;
            if (m_sp.getBoolean("hide_picture", false) || encodedDisplayPictureString.isEmpty()) {
                bitmap = null;
            } else {
                byte[] encodedDisplayPictureBytes = Base64.decode(encodedDisplayPictureString.getBytes(), Base64.DEFAULT);
                bitmap = BitmapFactory.decodeByteArray(encodedDisplayPictureBytes, 0, encodedDisplayPictureBytes.length);
            }
            updateScreen(true, playerId, pp, worldRank, countryRank, country, level, acc, playCount, totalHits, scores, bitmap, rankCounts);
        } else {
            // don't have player data cached
            // get data from servers and display it
            m_controller.onSettingsChanged(null, Constants.MODE_NONE);
            fetchData();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (!m_controller.getPlayerName().isEmpty()) {
            SharedPreferences.Editor editor = m_sp.edit();
            editor.putString(Constants.PLAYER_NAME, m_controller.getPlayerName());
            editor.putInt(Constants.MODE, m_controller.getMode());
            String[] recentSearches = m_controller.getRecentSearchesArray();
            Gson gson = new Gson();
            String recentSearchesJSON = gson.toJson(recentSearches, String[].class);
            editor.putString(Constants.RECENT_SEARCHES, recentSearchesJSON);

            // Cache data for next use
            if (m_cachedData != null) {
                try {
                    editor.putString(Constants.PLAYER_ID, m_cachedData.getPlayerId());
                    editor.putString(Constants.PP, m_cachedData.getPP());
                    editor.putString(Constants.WORLD_RANK, m_cachedData.getRank());
                    editor.putString(Constants.COUNTRY_RANK, m_cachedData.getCountryRank());
                    editor.putString(Constants.COUNTRY, m_cachedData.getCountry());
                    editor.putString(Constants.LEVEL, m_cachedData.getLevel());
                    editor.putString(Constants.ACC, m_cachedData.getAcc());
                    editor.putString(Constants.PLAY_COUNT, m_cachedData.getPlayCount());
                    editor.putString(Constants.TOTAL_HITS, m_cachedData.getTotalHits());
                    String rankCountsJSON = gson.toJson(m_cachedData.getRankCounts(), String[].class);
                    editor.putString(Constants.RANK_COUNTS, rankCountsJSON);
                    String scoresJSON = gson.toJson(m_cachedData.getScores(), Score[].class);
                    editor.putString(Constants.SCORES, scoresJSON);
                    //save display picture
                    if (m_cachedData.getDisplayPicture() != null) {
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        m_cachedData.getDisplayPicture().compress(Bitmap.CompressFormat.PNG, 100, baos);
                        byte[] b = baos.toByteArray();
                        String encodedDisplayPictureString = Base64.encodeToString(b, Base64.DEFAULT);
                        editor.putString(Constants.DISPLAY_PICTURE, encodedDisplayPictureString);
                    } else {
                        editor.putString(Constants.DISPLAY_PICTURE, ""); //put empty string when there's no bitmap to save
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            editor.apply();
        }
    }

    public void fetchData() {
        new MyAsyncTask().execute();
    }

    public void toggleUI() {
        NestedScrollView nestedScrollViewProfilePage = (NestedScrollView) getView().findViewById(R.id.nestedScrollViewProfilePage);
        RelativeLayout loadingPanel = (RelativeLayout) getActivity().findViewById(R.id.loadingPanel);

        ArrayList<View> viewsToToggle = new ArrayList<>();
        viewsToToggle.add(nestedScrollViewProfilePage);
        viewsToToggle.add(loadingPanel);
        for (View view : viewsToToggle
                ) {
            switch (view.getVisibility()) {
                case View.VISIBLE:
                    view.setVisibility(View.GONE);
                    break;
                case View.GONE:
                    view.setVisibility(View.VISIBLE);
                    break;
            }
        }

        ImageButton imageButton_refresh = (ImageButton) getActivity().findViewById(R.id.refresh_button);
        ImageButton button_find = (ImageButton) getActivity().findViewById(R.id.ibFind);
        MySpinner spinner_recentSearches = (MySpinner) getActivity().findViewById(R.id.spinnerRecentSearches);
        EditText editText_playerName = (EditText) getActivity().findViewById(R.id.etFindPlayerName);
        ImageButton imageButton_clearText = (ImageButton) getActivity().findViewById(R.id.ibFindClearText);
        if (imageButton_refresh.isEnabled()) {
            if (button_find != null) {
                editText_playerName.setEnabled(false);
                imageButton_clearText.setEnabled(false);
                spinner_recentSearches.setEnabled(false);
                button_find.setEnabled(false);
            }
            imageButton_refresh.setEnabled(false);
        } else {
            if (button_find != null) {
                if (m_isFetchDataSuccessful) {
                    // reset scrollview position to the top
                    nestedScrollViewProfilePage.scrollTo(0, 0);
                    // fade in the profile page - animation
                    Animation animation = AnimationUtils.loadAnimation(getContext(), android.R.anim.fade_in);
                    animation.setDuration(200);
                    nestedScrollViewProfilePage.startAnimation(animation);
                } else {
                    // revert mode selection if player name not found or bad connection
                    RadioGroup radioGroup = (RadioGroup) getView().findViewById(R.id.radio_group);
                    radioGroup.setOnCheckedChangeListener(null);
                    switch (m_controller.getMode()) {
                        case Constants.MODE_STD:
                            radioGroup.check(R.id.osu_std);
                            break;
                        case Constants.MODE_TAIKO:
                            radioGroup.check(R.id.osu_taiko);
                            break;
                        case Constants.MODE_CTB:
                            radioGroup.check(R.id.osu_ctb);
                            break;
                        case Constants.MODE_MANIA:
                            radioGroup.check(R.id.osu_mania);
                            break;
                        default:
                            break;
                    }
                    radioGroup.setOnCheckedChangeListener(m_onCheckedChangeListener);
                }
                editText_playerName.setEnabled(true);
                imageButton_clearText.setEnabled(true);
                spinner_recentSearches.setEnabled(true);
                button_find.setEnabled(true);
            }
            imageButton_refresh.setEnabled(true);
        }
    }

    public void updateScreen(boolean isOnStartUp, String playerId, String pp, String worldRank,
                             String countryRank, String country, String level, String acc,
                             String playCount, String totalHits, Score[] scores,
                             Bitmap displayPicture, String[] rankCounts) {
        TextView view_username = (TextView) getView().findViewById(R.id.textViewPlayerName);
        TextView view_PP = (TextView) getView().findViewById(R.id.textViewPP);
        TextView view_globalrank = (TextView) getView().findViewById(R.id.textViewGlobalRank);
        TextView view_countryrank = (TextView) getView().findViewById(R.id.textViewCountryRank);
        TextView view_acc = (TextView) getView().findViewById(R.id.textViewAccuracy);
        TextView view_playcount = (TextView) getView().findViewById(R.id.textViewPlayCount);
        TextView view_level = (TextView) getView().findViewById(R.id.textViewLevel);
        TextView view_totalhits = (TextView) getView().findViewById(R.id.textViewTotalHits);
        TextView view_rankcountss = (TextView) getView().findViewById(R.id.textViewRankCountSS);
        TextView view_rankcounts = (TextView) getView().findViewById(R.id.textViewRankCountS);
        TextView view_rankcounta = (TextView) getView().findViewById(R.id.textViewRankCountA);

        view_username.setText(m_controller.getPlayerName());
        view_PP.setText(pp + "pp");
        view_globalrank.setText("#" + worldRank + " in the World");
        view_countryrank.setText("#" + countryRank + " in " + country);
        view_acc.setText(acc + "% Acc");
        view_playcount.setText(playCount + " Play Count");
        view_level.setText("Lv. " + level);
        view_totalhits.setText(totalHits + " Total Hits");
        view_rankcountss.setText(rankCounts[0]);
        view_rankcounts.setText(rankCounts[1]);
        view_rankcounta.setText(rankCounts[2]);

        try {
            // set top plays
            ((TextView) getView().findViewById(R.id.textViewTopPlays)).setText(R.string.textViewTopPlays);
            if (scores.length == 0) {
                //display (none) if no scores are found
                getActivity().findViewById(R.id.recyclerViewScores).setVisibility(View.GONE);
                getActivity().findViewById(R.id.tvScoreNone).setVisibility(View.VISIBLE);
            } else {
                getActivity().findViewById(R.id.tvScoreNone).setVisibility(View.GONE);
                getActivity().findViewById(R.id.recyclerViewScores).setVisibility(View.VISIBLE);
                m_scoreRecyclerViewAdapter.updateScores(scores);
            }
            // set display picture
            ImageView imageView = (ImageView) getView().findViewById(R.id.imageViewPlayerPicture);
            if (displayPicture != null) {
                imageView.setImageBitmap(displayPicture);
            } else {
                imageView.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.no_display_picture));
            }

            // set favourites icon only after a server request, not on startup (or may get null references)
            // on startup, we wait for find frag to update us
            if (!isOnStartUp) {
                ImageButton ibFavourite = (ImageButton) getView().findViewById(R.id.ibFavourite);
                if (m_parentActivityCallback.isPlayerDataInFavouritesList(m_controller.getPlayerName(), playerId)) {
                    m_isFavourited = true;
                    ibFavourite.setImageResource(R.drawable.ic_profile_star_filled);
                } else {
                    m_isFavourited = false;
                    ibFavourite.setImageResource(R.drawable.ic_profile_star_empty);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateFavouritesIcon(boolean isFavourited) {
        ImageButton ibFavourite = (ImageButton) getView().findViewById(R.id.ibFavourite);
        m_isFavourited = isFavourited;
        if (isFavourited) {
            ibFavourite.setImageResource(R.drawable.ic_profile_star_filled);
        } else {
            ibFavourite.setImageResource(R.drawable.ic_profile_star_empty);
        }
    }

    public void toggleUIForFirstStart(boolean shouldShow) {
        if (shouldShow) {
            getView().findViewById(R.id.ibFavourite).setVisibility(View.VISIBLE);
            getView().findViewById(R.id.divider1).setVisibility(View.VISIBLE);
            getView().findViewById(R.id.divider2).setVisibility(View.VISIBLE);
            getView().findViewById(R.id.rlRankCount).setVisibility(View.VISIBLE);
        } else {
            getView().findViewById(R.id.ibFavourite).setVisibility(View.INVISIBLE);
            getView().findViewById(R.id.divider1).setVisibility(View.INVISIBLE);
            getView().findViewById(R.id.divider2).setVisibility(View.INVISIBLE);
            getView().findViewById(R.id.rlRankCount).setVisibility(View.INVISIBLE);
        }
    }

    public enum ResponseStatusForPlayerName {
        FOUND, NOTFOUND, UNKNOWN
    }

    public class MyAsyncTask extends AsyncTask<Void, Void, DataAccess> {
        @Override
        protected void onPreExecute() {
            toggleUI();
        }

        @Override
        protected DataAccess doInBackground(Void... params) {
            DataAccess data = new DataAccess(m_controller, this);
            try {
                boolean isValidData = data.askPeppyForData();
                // data is valid if player name exists
                if (isValidData) {
                    return data;
                } else { //player name does not exist
                    return null;
                }
            } catch (Exception e) {
                // an exception happened (could be connection, json related or other)
//                Log.e("Conn-Overall", e.toString());
                data.setHasConnectionIssue();
                return data;
            }
        }

        public void onPlayerNameValid() {
            // onProgressUpdate runs on UI thread and we have to update UI from that thread
            publishProgress();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // If user closes the app while we're updating UI on UI thread, we'll get null pointer exception.
            // Return if user closed the app already at this point.
            if (getActivity() == null) {
                return;
            }
            // Navigate to profile fragment for responsive UI design
            ViewPager viewPager = (ViewPager) getActivity().findViewById(R.id.viewpager);
            viewPager.setCurrentItem(0);
        }

        @Override
        protected void onPostExecute(DataAccess reply) {
            // If user closes the app while we're getting data, we'll get null pointer exception.
            // Return if user closed the app already at this point.
            if (getActivity() == null) {
                return;
            }

            if (reply == null) {  // player name does not exist
                Toast.makeText(getContext(), "Cannot find player", Toast.LENGTH_SHORT).show();
                m_controller.onSettingsReverted();
                m_isFetchDataSuccessful = false;
                // enable favourites and give not found feedback (profile frag -> main activity -> find frag)
                m_parentActivityCallback.onSearchFinished(ResponseStatusForPlayerName.NOTFOUND, m_controller.getStagingPlayername(), m_controller.getStagingPlayerId());
            } else if (reply.getHasConnectionIssue()) {  // connection timed out
                Toast.makeText(getContext(), "Connection problem", Toast.LENGTH_SHORT).show();
                m_controller.onSettingsReverted();
                m_isFetchDataSuccessful = false;
                m_parentActivityCallback.onSearchFinished(ResponseStatusForPlayerName.UNKNOWN, m_controller.getStagingPlayername(), m_controller.getStagingPlayerId());
            } else {  // got data successfully
                m_cachedData = reply; // save data on device cache
                try {
                    // update spinner values if new item added to recent searches list
                    boolean shouldUpdateSpinner = m_controller.onSettingsConfirmed(reply.getPlayerName(), m_controller.getStagingMode());
                    if (shouldUpdateSpinner) {
                        MySpinner mySpinner = (MySpinner) getActivity().findViewById(R.id.spinnerRecentSearches);
                        ArrayAdapter<String> arrayAdapter = (ArrayAdapter<String>) mySpinner.getAdapter();
                        arrayAdapter.clear();
                        arrayAdapter.addAll(m_controller.getRecentSearches());
                        arrayAdapter.notifyDataSetChanged();
                        mySpinner.setSelectionWithoutCallingListener(0);
                    }

                    // update screen contents (but still hidden)
                    updateScreen(false, reply.getPlayerId(), reply.getPP(), reply.getRank(),
                            reply.getCountryRank(), reply.getCountry(), reply.getLevel(),
                            reply.getAcc(), reply.getPlayCount(), reply.getTotalHits(),
                            reply.getScores(), reply.getDisplayPicture(), reply.getRankCounts());

                    // enable favourites and update name (profile frag -> main activity -> find frag)
                    m_parentActivityCallback.onSearchFinished(ResponseStatusForPlayerName.FOUND, reply.getPlayerName(), reply.getPlayerId());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                m_isFetchDataSuccessful = true;
            }
            // show screen
            toggleUI();
        }
    }
}
