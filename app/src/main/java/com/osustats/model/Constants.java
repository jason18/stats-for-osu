package com.osustats.model;

/**
 * Created by Jason on 2/14/2017.
 */

public class Constants {
    public static final String FIRST_START = "com.osustats.osustats.FIRST_START";

    public final static String PLAYER_NAME = "com.osustats.osustats.PLAYER_NAME";
    public final static String MODE = "com.osustats.osustats.MODE";
    public final static String PP = "com.osustats.osustats.PP";
    public final static String WORLD_RANK = "com.osustats.osustats.WORLD_RANK";
    public final static String COUNTRY_RANK = "com.osustats.osustats.COUNTRY_RANK";
    public final static String COUNTRY = "com.osustats.osustats.COUNTRY";
    public final static String LEVEL = "com.osustats.osustats.LEVEL";
    public final static String ACC = "com.osustats.osustats.ACC";
    public final static String PLAY_COUNT = "com.osustats.osustats.PLAY_COUNT";
    public final static String TOTAL_HITS = "com.osustats.osustats.TOTAL_HITS";
    public static final String RANK_COUNTS = "com.osustats.osustats.RANK_COUNTS";
    public final static String SCORES = "com.osustats.osustats.SCORES";
    public final static String RECENT_SEARCHES = "com.osustats.osustats.RECENT_SEARCHES";
    public final static String DISPLAY_PICTURE = "com.osustats.osustats.DISPLAY_PICTURE";
    public final static String FAVOURITES = "com.osustats.osustats.FAVOURITES";
    public static final String PLAYER_ID = "com.osustats.osustats.PLAYER_ID";

    public static final int MODE_STD = 0;
    public static final int MODE_TAIKO = 1;
    public static final int MODE_CTB = 2;
    public static final int MODE_MANIA = 3;
    public static final int MODE_NONE = -1;

    public static final int RECENT_SEARCHES_MAXIMUM = 5;
    public static final int FAVOURITES_MAXIMUM = 50;

    public static final String NOT_FOUND_PREFIX = "(.notfound#)";
    public static final int NOT_FOUND_PREFIX_LENGTH = 12;
}
