package com.osustats.model;

import java.util.Arrays;
import java.util.LinkedList;

/**
 * Created by Jason on 2/5/2016.
 */
public class Settings {

    private static Settings instance = null;
    private String playerName;
    private int mode = 0;
    private String stagingPlayerName;
    private String stagingUserId;
    private int stagingMode;
    private LinkedList<String> recentSearches = new LinkedList<>();
    private boolean shouldUpdateRecentSearches = true;

    private Settings() {
    }

    public static Settings getInstance() {
        if (instance == null) {
            instance = new Settings();
        }
        return instance;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public LinkedList<String> getRecentSearches() {
        return recentSearches;
    }

    public void setRecentSearches(String[] recentSearches) {
        this.recentSearches = new LinkedList<>(Arrays.asList(recentSearches));
    }

    public String[] getRecentSearchesArray() {
        String[] array = new String[recentSearches.size()];
        for (int i = 0; i < recentSearches.size(); i++) array[i] = recentSearches.get(i);
        return array;
    }

    public boolean getShouldUpdateRecentSearches() {
        return shouldUpdateRecentSearches;
    }

    public void setShouldUpdateRecentSearches(boolean shouldUpdateRecentSearches) {
        this.shouldUpdateRecentSearches = shouldUpdateRecentSearches;
    }

    public String getStagingPlayerName() {
        return stagingPlayerName;
    }

    public void setStagingPlayerName(String stagingPlayerName) {
        this.stagingPlayerName = stagingPlayerName;
    }

    public int getStagingMode() {
        return stagingMode;
    }

    public void setStagingMode(int stagingMode) {
        this.stagingMode = stagingMode;
    }

    public String getStagingUserId() {
        return stagingUserId;
    }

    public void setStagingUserId(String stagingUserId) {
        this.stagingUserId = stagingUserId;
    }

}
