package com.osustats.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.preference.PreferenceManager;

import com.osustats.controller.Controller;
import com.osustats.fragment.ProfileFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by Jason on 2/3/2016.
 */
public class DataAccess {

    private final static String ZERO = "0";
    private final static String DASH = "--";
    private final static String DASHSPACE = "-- ";
    private Controller mController;
    private JSONObject peppyServer_response;
    private JSONArray myServer_response;
    private Bitmap displayPicture;
    private boolean hasConnectionIssue;
    private ProfileFragment.MyAsyncTask asyncTaskContext;

    public DataAccess(Controller controller, ProfileFragment.MyAsyncTask asyncTask) {
        mController = controller;
        asyncTaskContext = asyncTask;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder result = new StringBuilder(inputStream.available());
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            result.append(line);
        }
        bufferedReader.close();
        return result.toString();
    }

    public boolean askPeppyForData() throws IOException, JSONException {
        final String charset = "UTF-8";
        final String key = "498aa4680179aca99cd787d1c9fcd6d336385a8c"; //TODO: hide this in a file when open source
        final String playerName = mController.getStagingPlayername();
        final String mode = Integer.toString(mController.getStagingMode());
        final String playerId = mController.getStagingPlayerId();
        final String typeString = "string";
        final String typeId = "id";

        HttpURLConnection connection = null;
        String responseString = "";

        // 1. Call get user from peppy's server
        for (int i = 0; i < 3; i++) {
            try {
                String peppyUrl = "https://osu.ppy.sh/api/get_user";
                String query;
                if (playerId.isEmpty()) {
                    query = String.format("k=%s&u=%s&m=%s&type=%s",
                            URLEncoder.encode(key, charset),
                            URLEncoder.encode(playerName, charset),
                            URLEncoder.encode(mode, charset),
                            URLEncoder.encode(typeString, charset));
                } else {
                    query = String.format("k=%s&u=%s&m=%s&type=%s",
                            URLEncoder.encode(key, charset),
                            URLEncoder.encode(playerId, charset),
                            URLEncoder.encode(mode, charset),
                            URLEncoder.encode(typeId, charset));
                }
                connection = (HttpURLConnection) new URL(peppyUrl + "?" + query).openConnection();
                // Connection timeout = 2 seconds
                connection.setConnectTimeout(2000);
                connection.setReadTimeout(2000);
                InputStream response = connection.getInputStream();
                responseString = convertInputStreamToString(response);
                break;
            } catch (IOException e) {
//                Log.e("Conn1", e.toString());
                if (i == 2) {
                    throw e;
                }
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }
        }

        JSONArray array = new JSONArray(responseString);
        if (!isValid(array)) {
            return false;
        }

        // notify UI thread immediately when we know player name exists
        asyncTaskContext.onPlayerNameValid();

        // 2. Call get user best from my server
        for (int i = 0; i < 3; i++) {
            try {
                peppyServer_response = array.getJSONObject(0);
                String myUrl = "http://statsserverv2-osustats.rhcloud.com/api/get_user_best";
                String limit = PreferenceManager.getDefaultSharedPreferences(mController.getProfileFragmentActivity()).getString("num_scores", "20");
                String query = String.format("user=%s&limit=%s&mode=%s",
                        URLEncoder.encode(playerName, charset),
                        URLEncoder.encode(limit, charset),
                        URLEncoder.encode(mode, charset));
                connection = (HttpURLConnection) new URL(myUrl + "?" + query).openConnection();
                // Connection timeout = 3 seconds
                connection.setReadTimeout(3000);
                connection.setConnectTimeout(3000);
                InputStream response = connection.getInputStream();
                responseString = convertInputStreamToString(response);
                break;
            } catch (IOException e) {
//                Log.e("Conn2", e.toString());
                if (i == 2) {
                    throw e;
                }
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }
        }

        myServer_response = (new JSONObject(responseString)).getJSONArray("info_array");

        // 3. Download player's display picture from peppy's server
        if (PreferenceManager.getDefaultSharedPreferences(mController.getProfileFragmentActivity()).getBoolean("hide_picture", false)) {
            displayPicture = null;
        } else {
            for (int i = 0; i < 3; i++) {
                try {
                    connection = (HttpURLConnection) new URL("http://s.ppy.sh/a/" + peppyServer_response.getString("user_id")).openConnection();
                    // Connection timeout = 2 seconds
                    connection.setReadTimeout(2000);
                    connection.setConnectTimeout(2000);
                    InputStream response = connection.getInputStream();
                    Bitmap bitmap = BitmapFactory.decodeStream(response);
                    float aspectRatio = bitmap.getHeight() / (float) bitmap.getWidth();
                    int height = 500;
                    int width = Math.round(height / aspectRatio);
                    displayPicture = Bitmap.createScaledBitmap(bitmap, width, height, false);
                    break;
                } catch (FileNotFoundException e) {
                    displayPicture = null;
                } catch (IOException e) {
//                    Log.e("Conn3", e.toString());
                    if (i == 2) {
                        throw e;
                    }
                } finally {
                    if (connection != null) {
                        connection.disconnect();
                    }
                }
            }
        }
        return true;
    }

    private boolean isValid(JSONArray array) {
        return array.length() != 0;
    }

    public String getPlayerName() throws JSONException {
        if (peppyServer_response.isNull("username")) {
            return DASH;
        }
        return peppyServer_response.getString("username");
    }

    public String getPlayerId() throws JSONException {
        if (peppyServer_response.isNull("user_id")) {
            return "";
        }
        return peppyServer_response.getString("user_id");
    }

    public String getPlayCount() throws JSONException {
        if (peppyServer_response.isNull("playcount")) {
            return ZERO;
        }
        return addCommas(peppyServer_response.getString("playcount"));
    }

    public String getPP() throws JSONException {
        if (peppyServer_response.isNull("pp_raw") || peppyServer_response.getString("pp_raw").equals("0")) {
            return DASHSPACE;
        }
        double pp = Double.parseDouble(peppyServer_response.getString("pp_raw"));
        pp = Math.round(pp * 100.0) / 100.0;
        return Double.toString(pp);
    }

    public String getAcc() throws JSONException {
        if (peppyServer_response.isNull("accuracy")) {
            return ZERO;
        }
        double acc = Double.parseDouble(peppyServer_response.getString("accuracy"));
        acc = Math.round(acc * 100.0) / 100.0;
        return Double.toString(acc);
    }

    public String getLevel() throws JSONException {
        if (peppyServer_response.isNull("level")) {
            return ZERO;
        }
        double level = Double.parseDouble(peppyServer_response.getString("level"));
        level = Math.round(level * 100.0) / 100.0;
        return Double.toString(level);
    }

    public String getRank() throws JSONException {
        if (peppyServer_response.isNull("pp_rank") || getPP().equals(DASHSPACE)) {
            return DASH;
        }
        return addCommas(peppyServer_response.getString("pp_rank"));
    }

    public String getCountry() throws JSONException {
        if (peppyServer_response.isNull("country")) {
            return DASH;
        }
        return mController.getCountryFullName(peppyServer_response.getString("country"));
    }

    public String getCountryRank() throws JSONException {
        if (peppyServer_response.isNull("pp_country_rank") || getPP().equals(DASHSPACE)) {
            return DASH;
        }
        return addCommas(peppyServer_response.getString("pp_country_rank"));
    }

    public String getTotalHits() throws JSONException {
        if (peppyServer_response.isNull("count300") && peppyServer_response.isNull("count100") && peppyServer_response.isNull("count50")) {
            return ZERO;
        }
        int totalHits = Integer.parseInt(peppyServer_response.getString("count300")) + Integer.parseInt(peppyServer_response.getString("count100"))
                + Integer.parseInt(peppyServer_response.getString("count50"));
        return addCommas(String.valueOf(totalHits));
    }

    public String[] getRankCounts() throws JSONException {
        if (peppyServer_response.isNull("count_rank_ss") || peppyServer_response.isNull("count_rank_s") || peppyServer_response.isNull("count_rank_a")) {
            String[] zeroArray = {"0", "0", "0"};
            return zeroArray;
        }
        return new String[]{peppyServer_response.getString("count_rank_ss"), peppyServer_response.getString("count_rank_s"), peppyServer_response.getString("count_rank_a")};
    }

    private String addCommas(String num) {
        StringBuilder sb = new StringBuilder(num);
        for (int i = num.length() - 3; i > 0; i = i - 3) {
            sb.insert(i, ',');
        }
        return sb.toString();
    }

    public Score[] getScores() throws JSONException {
        Score[] scores = new Score[myServer_response.length()];
        for (int i = 0; i < myServer_response.length(); i++) {
            JSONObject JSONscore = myServer_response.getJSONObject(i);
            Score score = new Score();
            score.acc = JSONscore.getString("acc");
            score.artist = JSONscore.getString("artist");
            score.beatmap_id = JSONscore.getString("beatmap_id");
            score.diff_rating = JSONscore.getString("diff_rating");
            score.enabled_mods = JSONscore.getString("enabled_mods");
            score.how_long_ago = JSONscore.getString("how_long_ago");
            score.max_combo = JSONscore.getString("max_combo");
            score.pp = JSONscore.getString("pp");
            score.rank = JSONscore.getString("rank");
            score.title = JSONscore.getString("title");
            score.user_combo = JSONscore.getString("user_combo");
            score.version = JSONscore.getString("version");
            scores[i] = score;
        }
        return scores;
    }

    // Support showing connectivity issue
    public void setHasConnectionIssue() {
        hasConnectionIssue = true;
    }

    public boolean getHasConnectionIssue() {
        return hasConnectionIssue;
    }

    public Bitmap getDisplayPicture() {
        return displayPicture;
    }
}
