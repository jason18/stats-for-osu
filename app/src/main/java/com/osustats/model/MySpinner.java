package com.osustats.model;

import android.content.Context;
import android.support.v7.widget.AppCompatSpinner;
import android.util.AttributeSet;

/**
 * Created by Jason on 4/15/2017.
 */

public class MySpinner extends AppCompatSpinner {

    public MySpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setSelection(int position) {
        boolean sameSelected = position == getSelectedItemPosition();
        super.setSelection(position);
        if (sameSelected && getOnItemSelectedListener() != null) {
            // Spinner does not call OnItemSelectedListener if same item is selected, so do it manually
            getOnItemSelectedListener().onItemSelected(this, getSelectedView(), position, getSelectedItemId());
        }
    }

    // Hack to prevent OnItemSelectedListener to fire infinite number of times when setSelection() gets called
    // Disable OnItemSelectedListener, setSelection, then enable OnItemSelectedListener
    public void setSelectionWithoutCallingListener(final int position) {
        final OnItemSelectedListener onItemSelectedListener = this.getOnItemSelectedListener();
        this.setOnItemSelectedListener(null);
        this.post(new Runnable() {
            @Override
            public void run() {
                MySpinner.this.setSelection(position);
                MySpinner.this.post(new Runnable() {
                    @Override
                    public void run() {
                        MySpinner.this.setOnItemSelectedListener(onItemSelectedListener);
                    }
                });
            }
        });
    }

}
