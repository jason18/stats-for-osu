package com.osustats.model;

import android.os.AsyncTask;

import com.osustats.controller.Controller;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

/**
 * Created by Jason on 2/10/2016.
 */
public class CountryConversion {

    private Controller mController;
    private HashMap<String, String> countryMap;
    private boolean isDoneProcessing;

    public CountryConversion(Controller controller) {
        countryMap = new HashMap<>();
        isDoneProcessing = false;
        mController = controller;
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(mController.getProfileFragmentActivity().getResources().getAssets().open("country_code.json")));
                    String s = bufferedReader.readLine();
                    JSONArray jsonArray = new JSONArray(s);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        int j = jsonArray.length();
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        countryMap.put(jsonObject.getString("Code"), jsonObject.getString("Name"));
                    }
                    bufferedReader.close();
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                isDoneProcessing = true;
            }
        }.execute();
    }

    public String convertToFullName(String input) {
        if (isDoneProcessing && countryMap.containsKey(input)) {
            return countryMap.get(input);
        } else {
            return input;
        }
    }
}
