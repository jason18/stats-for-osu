package com.osustats.model;

public class Score {
    public String acc;
    public String artist;
    public String beatmap_id;
    public String diff_rating;
    public String enabled_mods;
    public String how_long_ago;
    public String max_combo;
    public String pp;
    public String rank;
    public String title;
    public String user_combo;
    public String version;
}
