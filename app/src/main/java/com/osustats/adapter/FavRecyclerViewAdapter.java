package com.osustats.adapter;

import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.osustats.controller.Controller;
import com.osustats.interfaces.ActivityFragmentCommunication;
import com.osustats.model.Constants;
import com.osustats.osustats.R;

import java.util.LinkedList;

/**
 * Created by Jason on 5/13/2017.
 */

public class FavRecyclerViewAdapter extends RecyclerView.Adapter<FavRecyclerViewAdapter.ViewHolder> {

    private LinkedList<PlayerData> m_playerData;
    private Controller m_controller;
    private boolean m_shouldShowButtons = false;
    private boolean m_clickable = true;
    private ActivityFragmentCommunication m_parentActivityCallback;

    // Provide a suitable constructor (depends on the kind of dataset)
    public FavRecyclerViewAdapter(LinkedList<PlayerData> linkedList, Controller controller, ActivityFragmentCommunication parentActivityCallback) {
        m_playerData = linkedList;
        m_parentActivityCallback = parentActivityCallback;
        m_controller = controller;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public FavRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        RelativeLayout view = (RelativeLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.favourite_item, parent, false);
        // set the view's size, margins, paddings and layout parameters
        // ...
        final ViewHolder vh = new ViewHolder(view);
        // set search player functionality
        vh.m_relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String idToSearchFor = m_playerData.get(vh.getAdapterPosition()).playerId;
                String nameToSearchFor = m_playerData.get(vh.getAdapterPosition()).playerName;
                if (nameToSearchFor.startsWith(Constants.NOT_FOUND_PREFIX)) {
                    nameToSearchFor = nameToSearchFor.substring(Constants.NOT_FOUND_PREFIX_LENGTH);
                }
                m_controller.onSettingsChanged(nameToSearchFor, idToSearchFor, Constants.MODE_NONE, false);
                m_parentActivityCallback.onSearch();
            }
        });
        // set move up player functionality
        vh.m_relativeLayout.findViewById(R.id.ibMoveUp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = vh.getAdapterPosition();
                if (position != 0) {
                    m_playerData.add(position - 1, m_playerData.get(position));
                    m_playerData.remove(position + 1);
                    notifyItemRangeChanged(position - 1, 2);
                }
            }
        });
        // set move down player functionality
        vh.m_relativeLayout.findViewById(R.id.ibMoveDown).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = vh.getAdapterPosition();
                if (position != getItemCount() - 1) {
                    m_playerData.add(position + 2, m_playerData.get(position));
                    m_playerData.remove(position);
                    notifyItemRangeChanged(position, 2);
                }
            }
        });
        // set remove player functionality
        vh.m_relativeLayout.findViewById(R.id.ibRemove).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // reflect change in profile star icon
                if (m_controller.getPlayerName().compareToIgnoreCase(m_playerData.get(vh.getAdapterPosition()).playerName) == 0) {
                    m_parentActivityCallback.updateFavouritesIcon(false);
                }
                m_playerData.remove(vh.getAdapterPosition());
                notifyItemRemoved(vh.getAdapterPosition());
            }
        });
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        RelativeLayout relativeLayoutFavItem = holder.m_relativeLayout;

        // change not found names to red
        TextView tvPlayerName = (TextView) relativeLayoutFavItem.findViewById(R.id.tvPlayerName);
        if (m_playerData.get(position).playerName.startsWith(Constants.NOT_FOUND_PREFIX)) {
            tvPlayerName.setTextColor(Color.RED);
            tvPlayerName.setText(m_playerData.get(position).playerName.substring(Constants.NOT_FOUND_PREFIX_LENGTH));
        } else {
            tvPlayerName.setTextColor(ContextCompat.getColor(relativeLayoutFavItem.getContext(), R.color.colorContent));
            tvPlayerName.setText(m_playerData.get(position).playerName);
        }

        if (m_shouldShowButtons) {
            relativeLayoutFavItem.findViewById(R.id.ibMoveUp).setVisibility(View.VISIBLE);
            relativeLayoutFavItem.findViewById(R.id.ibMoveDown).setVisibility(View.VISIBLE);
            relativeLayoutFavItem.findViewById(R.id.ibRemove).setVisibility(View.VISIBLE);
        } else {
            relativeLayoutFavItem.findViewById(R.id.ibMoveUp).setVisibility(View.GONE);
            relativeLayoutFavItem.findViewById(R.id.ibMoveDown).setVisibility(View.GONE);
            relativeLayoutFavItem.findViewById(R.id.ibRemove).setVisibility(View.GONE);
        }
        relativeLayoutFavItem.setEnabled(m_clickable);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return m_playerData.size();
    }

    public void addPlayer(String playerName, String playerId) {
        PlayerData playerData = new PlayerData(playerName, playerId);
        m_playerData.addLast(playerData);
        notifyItemInserted(m_playerData.size() - 1);
    }

    public void hideButtons() {
        m_shouldShowButtons = false;
        notifyDataSetChanged();
    }

    public void showButtons() {
        m_shouldShowButtons = true;
        notifyDataSetChanged();
    }

    public PlayerData[] getPlayerDataArray() {
        PlayerData[] array = new PlayerData[m_playerData.size()];
        for (int i = 0; i < m_playerData.size(); i++) array[i] = m_playerData.get(i);
        return array;
    }

    // enable clicking for all recycler view items
    public void enableClicking() {
        m_clickable = true;
        notifyDataSetChanged();
    }

    // disable clicking for all recycler view items
    public void disableClicking() {
        m_clickable = false;
        notifyDataSetChanged();
    }

    // update existing player in favourites list
    public void updatePlayerInFavourites(boolean isPlayerNameFound, String playerName, String playerId) {
        int entryIndex = playerDataContainsIgnoreCase(playerName, playerId);
        if (entryIndex != -1) {
            if (isPlayerNameFound) {
                PlayerData entry = m_playerData.get(entryIndex);
                entry.playerName = playerName;
                entry.playerId = playerId;
            } else {
                PlayerData entry = m_playerData.get(entryIndex);
                entry.playerName = Constants.NOT_FOUND_PREFIX + playerName;
                entry.playerId = playerId;
            }
            notifyItemChanged(entryIndex);
        }
    }

    // add or remove player in favourites list
    public void updateFavouritesList(boolean isFavourited, String playerName, String playerId) {
        if (isFavourited) {
            int index = playerDataContainsIgnoreCase(playerName, playerId);
            // if player is not in fav list already, add it
            if (index == -1) {
                addPlayer(playerName, playerId);
            }
        } else {
            int index = playerDataContainsIgnoreCase(playerName, playerId);
            // if player is in fav list, remove it
            if (index != -1) {
                m_playerData.remove(index);
                notifyItemRemoved(index);
            }
        }
    }

    // returns index of matching player name, -1 if none
    // check id first, then check name
    public int playerDataContainsIgnoreCase(String name, String id) {
        for (int i = 0; i < m_playerData.size(); i++) {
            // Check id
            if (!id.isEmpty()) {
                String playerId = m_playerData.get(i).playerId;
                if (playerId.equals(id)) {
                    return i;
                }
            }
            // Check name
            String playerName = m_playerData.get(i).playerName;
            if (playerName.startsWith(Constants.NOT_FOUND_PREFIX)) {
                playerName = playerName.substring(Constants.NOT_FOUND_PREFIX_LENGTH);
            }
            if (playerName.compareToIgnoreCase(name) == 0) {
                return i;
            }
        }
        return -1;
    }

    public static class PlayerData {
        String playerName;
        String playerId;

        public PlayerData(String playerName, String playerId) {
            this.playerName = playerName;
            this.playerId = playerId;
        }
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout m_relativeLayout;

        public ViewHolder(RelativeLayout relativeLayout) {
            super(relativeLayout);
            m_relativeLayout = relativeLayout;
        }
    }
}