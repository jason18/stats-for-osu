package com.osustats.adapter;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.osustats.model.Score;
import com.osustats.osustats.R;

/**
 * Created by Jason on 5/13/2017.
 */

public class ScoreRecyclerViewAdapter extends RecyclerView.Adapter<ScoreRecyclerViewAdapter.ViewHolder> {
    private Score[] m_scores;
    private SharedPreferences m_sp;

    // Provide a suitable constructor (depends on the kind of dataset)
    public ScoreRecyclerViewAdapter(Score[] scores, View view) {
        m_scores = scores;
        m_sp = PreferenceManager.getDefaultSharedPreferences(view.getContext());
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ScoreRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        RelativeLayout v = (RelativeLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.score, parent, false);
        // set the view's size, margins, paddings and layout parameters
        // ...
        final ViewHolder vh = new ViewHolder(v);
        // set score's button to point to osu website map page (put this here instead of onBindViewHolder() to improve performance)
        vh.m_relativeLayout.findViewById(R.id.ivVisitWebsite).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Score score = m_scores[vh.getAdapterPosition()];
                String url;
                if (m_sp.getBoolean("visit_new_website", true)) {
                    url = "https://osu.ppy.sh/beatmaps/" + score.beatmap_id;
                } else {
                    url = "https://osu.ppy.sh/b/" + score.beatmap_id;
                }
                CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                builder.setToolbarColor(ContextCompat.getColor(vh.m_relativeLayout.getContext(), R.color.colorBackground));
                CustomTabsIntent customTabsIntent = builder.build();
                customTabsIntent.launchUrl(vh.m_relativeLayout.getContext(), Uri.parse(url));
            }
        });
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Score score = m_scores[position];
        final RelativeLayout relativeLayoutScore = holder.m_relativeLayout;
        // set score's index
        ((TextView) relativeLayoutScore.findViewById(R.id.tvScoreIndex)).setText(String.valueOf(position + 1) + ".");
        // set score's rank image
        setRankImage(relativeLayoutScore, score.rank);
        // set score's details
        setScoreData(relativeLayoutScore, score);
        // set score's highlight if score set recently (<= 3 days ago)
        String howLongAgo = score.how_long_ago;
        String[] howLongAgoBits = howLongAgo.split(" ");
        if (howLongAgoBits[0].startsWith("just") || howLongAgoBits[1].startsWith("minute") || howLongAgoBits[1].startsWith("hour") ||
                (howLongAgoBits[1].startsWith("day") && Integer.valueOf(howLongAgoBits[0]) <= 3)) {
            relativeLayoutScore.setBackground(ContextCompat.getDrawable(relativeLayoutScore.getContext(), R.drawable.score_background_selector));
        } else {
            relativeLayoutScore.setBackgroundResource(0);
        }
    }

    private void setScoreData(RelativeLayout relativeLayoutScore, Score score) {
        // set first row text
        String firstPart = score.title + " [" + score.version + "]";
        String secondPart = score.enabled_mods.equals("NoMod") ? "" : " +" + score.enabled_mods;
        SpannableString spannableString = new SpannableString(firstPart + secondPart);
        spannableString.setSpan(new StyleSpan(Typeface.BOLD), firstPart.length(), spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        ((TextView) relativeLayoutScore.findViewById(R.id.tvScoreFirstRow)).setText(spannableString);

        // set second row text
        ((TextView) relativeLayoutScore.findViewById(R.id.tvScoreSecondRow)).setText(score.artist);

        // set third row text
        // round pp according to settings
        boolean isRounded = PreferenceManager.getDefaultSharedPreferences(relativeLayoutScore.getContext()).getBoolean("pp_rounding", false);
        if (isRounded) {
            score.pp = String.valueOf(Math.round(Float.parseFloat(score.pp)));
        }
        String data = score.user_combo + (score.max_combo.equals("-1") ? "x  " : ("/" + score.max_combo + "x  ")) + score.acc + "  " + score.pp + "pp";
        ((TextView) relativeLayoutScore.findViewById(R.id.tvScoreThirdRow)).setText(data);

        // set fourth row text
        ((TextView) relativeLayoutScore.findViewById(R.id.tvScoreFourthRowLeft)).setText(score.diff_rating);
        ((TextView) relativeLayoutScore.findViewById(R.id.tvScoreFourthRowRight)).setText(score.how_long_ago);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return m_scores.length;
    }

    public void updateScores(Score[] scores) {
        m_scores = scores;
        notifyDataSetChanged();
    }

    // set score's rank image
    private void setRankImage(RelativeLayout relativeLayoutScore, String rank) {
        ImageView imageViewRank = (ImageView) relativeLayoutScore.findViewById(R.id.ivRankImage);
        switch (rank) {
            case "XH":
                imageViewRank.setImageResource(R.drawable.ranking_xh_1);
                break;
            case "X":
                imageViewRank.setImageResource(R.drawable.ranking_x_1);
                break;
            case "SH":
                imageViewRank.setImageResource(R.drawable.ranking_sh_1);
                break;
            case "S":
                imageViewRank.setImageResource(R.drawable.ranking_s_1);
                break;
            case "A":
                imageViewRank.setImageResource(R.drawable.ranking_a_1);
                break;
            case "B":
                imageViewRank.setImageResource(R.drawable.ranking_b_1);
                break;
            case "C":
                imageViewRank.setImageResource(R.drawable.ranking_c_1);
                break;
            case "D":
                imageViewRank.setImageResource(R.drawable.ranking_d_1);
                break;
            default:
                break;
        }
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout m_relativeLayout;

        public ViewHolder(RelativeLayout relativeLayout) {
            super(relativeLayout);
            m_relativeLayout = relativeLayout;
        }
    }
}