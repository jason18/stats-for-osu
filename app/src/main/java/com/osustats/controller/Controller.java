package com.osustats.controller;

import android.support.v4.app.FragmentActivity;

import com.osustats.fragment.ProfileFragment;
import com.osustats.model.Constants;
import com.osustats.model.CountryConversion;
import com.osustats.model.Settings;

import java.util.LinkedList;

/**
 * Created by Jason on 2/5/2016.
 */
public class Controller {

    // View reference
    private ProfileFragment mProfileFragment;
    // Model reference
    private Settings mSettings;
    private CountryConversion mCountryConversion;

    // called in FindFragment
    public Controller() {
        mProfileFragment = null;
        mCountryConversion = null;
        mSettings = Settings.getInstance();
    }

    // called in ProfileFragment
    public Controller(ProfileFragment profileFragment) {
        mProfileFragment = profileFragment;
        mCountryConversion = new CountryConversion(this);
        mSettings = Settings.getInstance();
    }

    // Called when main activity starts/resumes
    public void onSettingsInitialized(String username, int mode, String[] recentSearches) {
        mSettings.setPlayerName(username);
        mSettings.setMode(mode);
        if (recentSearches != null) {
            mSettings.setRecentSearches(recentSearches);
        }
    }

    // Called when settings is changed (via find button or name click)
    // We fetch data from servers
    public void onSettingsChanged(String username, int mode) {
        if (username != null) {
            mSettings.setStagingPlayerName(username);
        } else {
            mSettings.setStagingPlayerName(mSettings.getPlayerName());
        }

        if (mode != Constants.MODE_NONE) {
            mSettings.setStagingMode(mode);
        } else {
            mSettings.setStagingMode(mSettings.getMode());
        }
        mSettings.setStagingUserId("");
    }

    // Called when we do NOT want to update spinner after fetching data - clicking on favourites list e.g.
    public void onSettingsChanged(String username, String userId, int mode, boolean shouldUpdateRecentSearches) {
        onSettingsChanged(username, mode);
        if (shouldUpdateRecentSearches == false) {
            mSettings.setShouldUpdateRecentSearches(shouldUpdateRecentSearches);
        }
        if (!userId.isEmpty()) {
            mSettings.setStagingUserId(userId);
        }
    }

    // Called when we get data from peppy
    // Change name in settings object to the correct player name from peppy
    // Also maintains the recent searches linked list
    // Returns whether spinner needs to update itself because its contents - the recent searches - have changed
    public boolean onSettingsConfirmed(String username, int mode) {
        mSettings.setPlayerName(username);
        mSettings.setMode(mode);

        boolean shouldUpdateRecentSearches = mSettings.getShouldUpdateRecentSearches();
        resetShouldUpdateRecentSearches();
        if (shouldUpdateRecentSearches) {
            LinkedList<String> linkedList = mSettings.getRecentSearches();
            if (!linkedList.contains(username)) {
                if (linkedList.size() >= Constants.RECENT_SEARCHES_MAXIMUM) { // max of 5 recent searches
                    linkedList.removeLast();
                }
                linkedList.addFirst(username);
                return true;
            }
        }
        return false;
    }

    // Called when user name is invalid or there is a connection problem
    public void onSettingsReverted() {
        resetShouldUpdateRecentSearches();
    }

    private void resetShouldUpdateRecentSearches() {
        mSettings.setShouldUpdateRecentSearches(true);
    }

    public String getPlayerName() {
        return mSettings.getPlayerName();
    }

    public int getMode() {
        return mSettings.getMode();
    }

    public FragmentActivity getProfileFragmentActivity() {
        return mProfileFragment.getActivity();
    }

    public String getCountryFullName(String input) {
        return mCountryConversion.convertToFullName(input);
    }

    public LinkedList<String> getRecentSearches() {
        return mSettings.getRecentSearches();
    }

    public String[] getRecentSearchesArray() {
        return mSettings.getRecentSearchesArray();
    }

    public String getStagingPlayername() {
        return mSettings.getStagingPlayerName();
    }

    public int getStagingMode() {
        return mSettings.getStagingMode();
    }

    public String getStagingPlayerId() {
        return mSettings.getStagingUserId();
    }
}
