package com.osustats.interfaces;

import com.osustats.fragment.ProfileFragment.ResponseStatusForPlayerName;

/**
 * Created by Jason on 6/24/2017.
 */

public interface ActivityFragmentCommunication {
    void onSearch();

    void onSearchFinished(ResponseStatusForPlayerName responseStatusForPlayerName, String playerName, String playerId);

    void updateFavouritesIcon(boolean isFavourited);

    void onFavouritesIconClicked(boolean isFavourited, String playerName, String playerId);

    boolean isPlayerDataInFavouritesList(String playerName, String playerId);
}
